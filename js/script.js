

const btn = document.getElementById('btn');

class IPInfo {

    async searchIp() {

        try {

            const urlSearcher = await fetch('https://api.ipify.org/?format=json');
            const ipData = await urlSearcher.json();
            const ipAddress = ipData.ip;

            const ipInfoResponse = await fetch(`http://ip-api.com/json/${ipAddress}?fields=continent,country,regionName,city,district,query`);
            const ipInfoData = await ipInfoResponse.json();

            return {
                query: ipInfoData.query,
                continent: ipInfoData.continent,
                country: ipInfoData.country,
                region: ipInfoData.regionName,
                city: ipInfoData.city,
                district: ipInfoData.district,
            };
        } catch (error) {
            console.log('Error', error);
            return null;
        }
    }
}

btn.addEventListener('click', async () => {

    const ipInfo = new IPInfo();
    const data = await ipInfo.searchIp();

    if (data) {
        const container = document.querySelector('.container');
        container.innerHTML = `
        <h2>Ваш IP-адрес: ${data.query}</h2>
        <p>Континент: ${data.continent}</p>
        <p>Країна: ${data.country}</p>
        <p>Регіон: ${data.region}</p>
        <p>Місто: ${data.city}</p>
        <p>Район: ${data.district}</p>
        `
    } else {
        alert('Помилка отримання IP');
    }
})
